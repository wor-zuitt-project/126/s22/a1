
// 1


db.channels.insertMany([
	{
		"channelName": "Red Team",
		"name": [ "Red1", "Red2", "Red3" ]
	}, 
	{
		"channelName": "Blue Team",
		"name": [ "Blue1", "Blue2", "Blue3" ]
	},
	{
		"channelName": "Yellow Team",
		"name": [ "Yellow1", "Yellow2", "Yellow3" ]

	},
	{
		"channelName": "Black Team",
		"name": [ "Black1", "Black2", "Black3" ]
	},
	{
		"channelName": "White Team",
		"name": [ "White1", "White2", "White3" ]
	}
])


//2 


db.channels.updateMany(
	{ },
	{
		$set: {
			"isActive": true
		}
	}

)


//3


db.channels.updateOne(
	{"channelName" : "Black Team" },
	{
		$set: {
			"isActive": false
		}
	}

)


db.channels.updateOne(
	{"channelName" : "Yellow Team" },
	{
		$set: {
			"isActive": false
		}
	}

)




//4

db.channels.deleteOne({
	"channelName" : "White Team"
})

//5

db.channels.find({"isActive": true})

//6

db.channels.deleteMany({
	"isActive": false
})